(function() {
  var FX = {
      easing: {
          linear: function(progress) {
              return progress;
          },
          quadratic: function(progress) {
              return Math.pow(progress, 2);
          },
          swing: function(progress) {
              return 0.5 - Math.cos(progress * Math.PI) / 2;
          },
          circ: function(progress) {
              return 1 - Math.sin(Math.acos(progress));
          },
          back: function(progress, x) {
              return Math.pow(progress, 2) * ((x + 1) * progress - x);
          },
          bounce: function(progress) {
              for (var a = 0, b = 1, result; 1; a += b, b /= 2) {
                  if (progress >= (7 - 4 * a) / 11) {
                      return -Math.pow((11 - 6 * a - 11 * progress) / 4, 2) + Math.pow(b, 2);
                  }
              }
          },
          elastic: function(progress, x) {
              return Math.pow(2, 10 * (progress - 1)) * Math.cos(20 * Math.PI * x / 3 * progress);
          }
      },
      animate: function(options) {
          var start = new Date();
          var id = setInterval(function() {
              var timePassed = new Date() - start;
              var progress = timePassed / options.duration;
              if (progress > 1) {
                  progress = 1;
              }
              options.progress = progress;
              var delta = options.delta(progress);
              options.step(delta);
              if (progress == 1) {
                  clearInterval(id);
                  options.complete();
              }
          }, options.delay || 10);
      },
      fadeOut: function(element, options) {
          var to = 1;
          this.animate({
              duration: options.duration,
              delta: function(progress) {
                  progress = this.progress;
                  return FX.easing.swing(progress);
              },
              complete: options.complete,
              step: function(delta) {
                  element.style.opacity = to - delta;
              }
          });
      },
      fadeIn: function(element, options) {
          var to = 0;
          this.animate({
              duration: options.duration,
              delta: function(progress) {
                  progress = this.progress;
                  return FX.easing.swing(progress);
              },
              complete: options.complete,
              step: function(delta) {
                  element.style.opacity = to + delta;
              }
          });
      }
  };
  window.FX = FX;
})();

var eatToDeadApp = angular.module("eatToDeadApp", ["firebase", "ui.bootstrap"])
                          .filter('latestNames', function() {
                            return function(names) {
                              if(names.length <= 2)
                                return (names + " 很幹");
                              else
                                return (names.slice(-3) + "... 很幹");
                            };
                          });

eatToDeadApp.controller("MainCtrl", function($firebase) {
  var vm = this;

  var REF = "https://blinding-heat-1250.firebaseio.com";
  var USERS_REF = "https://blinding-heat-1250.firebaseio.com/users/";
  var PRODUCTS_REF = "https://blinding-heat-1250.firebaseio.com/products/";
  var LIST_REF = "https://blinding-heat-1250.firebaseio.com/products/list/";
  var NUM_PER_PAGE = 10;

  vm.isLogin = false;
  vm.isCollapsed = true;
  vm.isAlert = false;
  vm.isOther = false;

  var ref = new Firebase(REF);
  
  vm.loginFB = function() {
    ref.authWithOAuthPopup("facebook", function(error, authData) {
      if (authData) {
        // the access token will allow us to make Open Graph API calls
        //console.log(authData.facebook.accessToken);
        ref.authWithOAuthRedirect("facebook", function(error) {});
      }
    }, {
      scope: "email,user_likes" // the permissions requested
    });
  };

  vm.logoutFB = function() {
    ref.unauth();
    vm.isLogin = false;
  };

  var uid;
  var name;
  var onAuthChange = function(authData) {
    var usersRef = new Firebase(USERS_REF);

    var checkIfUserExists = function(authData) {
      var users = $firebase(usersRef).$asArray();
      users.$loaded().then(function() {
        if(!users.$getRecord(authData.uid))
          userExistsCallback(authData);
        else {
          uid = authData.uid;
          name = authData.facebook.displayName;
          vm.isLogin = true;
        }
      });
    };

    var userExistsCallback = function(authData) {
      var users = $firebase(usersRef).$asObject();
      users.$loaded().then(function() {
        users[authData.uid] = authData;
        users.$save().then(function(ref) {
          uid = authData.uid;
          name = authData.facebook.displayName;
          vm.isLogin = true;
        }/*, optional error callback */);
      });
    };

    if(authData && !vm.isLogin) {
      checkIfUserExists(authData);
      
    }
  };

  ref.onAuth(onAuthChange);
  ref.offAuth(onAuthChange);

  vm.closeAlert = function() {
    vm.isAlert = false;
  };

  vm.setOther = function() {
    vm.isOther = !vm.isOther;
    vm.otherBrand = "";
  };

  vm.submit = function() {
    if(!vm.pBrand && !vm.otherBrand) {
      vm.isAlert = true;
      return;
    }
    else if(!vm.pName)
      return;

    if(vm.isOther)
      vm.pBrand = vm.otherBrand;
    var productsRef = new Firebase(PRODUCTS_REF);
    var products = $firebase(productsRef).$asObject();
    products.$loaded().then(function() {
      var listRef = new Firebase(LIST_REF);
      var list = $firebase(listRef).$asArray();
      list.$loaded().then(function() {
        list.$add({pName: vm.pName, pBrand: vm.pBrand, fuck: 1, users: [uid], names: [name]})
            .then(function(ref) {
              vm.filteredProducts = vm.products;
              vm.totalItems = vm.products.length;
              vm.currentPage = Math.ceil(vm.totalItems/vm.numPerPage);
              vm.pageChanged();

              vm.clear();
              vm.isCollapsed = !vm.isCollapsed;
              vm.isAlert = false;
              vm.isOther = false;
            }/*, optional error callback */);
      });

      products.count += 1;
      products.$save().then(function(ref) { }/*, optional error callback */);
    });
  };
  
  vm.clear = function() {
    vm.pName = "";
    vm.pBrand = "";
  };

  var listRef = new Firebase(LIST_REF);
  vm.names;
  vm.products = $firebase(listRef).$asArray();
  vm.products.$loaded().then(function() {
    vm.totalItems = vm.products.length;
    vm.order("-fuck");
  });
  vm.products.$watch(function() {
    vm.products.forEach(arrayElements);
  });
  var arrayElements = function(element, index, array) {
    
  };

  vm.sizeClass = function(item) {
    var class_name = "sc--fs9";
    if(item.fuck == 10)
      class_name = "sc--fs12";
    if(item.fuck == 100)
      class_name = "sc--fs15";
    if(item.fuck == 500)
      class_name = "sc--fs18";
    if(item.fuck == 1000)
      class_name = "sc--fs21";
    if(item.fuck == 6000)
      class_name = "sc--fs24";
    
    return class_name;

    /*
      0                   (不顯示)
      1~10             灰數字
      11~49            黃數字
      50~99            紅數字
      100~999        白      HOT
      1000~1999     白爆  爆!
      2000~4999     紅爆  爆!
      5000~9999     藍爆  爆!
      10000~29999  青爆  爆!
      30000~59999  綠爆  爆!
      60000~99999  黃爆  爆!
      ≧100000        紫爆  爆!
    */
  };
  
  vm.addFuck = function(item, $event) {
    item.fuck = (item.fuck||0) + 1;
    FX.fadeIn($event.target, {
        duration: 1000,
        complete: function() {
            //alert('Complete');
        }
    });

    item.users.push(uid);
    item.names.push(name);
    vm.products.$save(item).then(function(ref) {});
  };

  vm.isDisabled = function(users) {
    if(!vm.isLogin || users.indexOf(uid) > -1)
      return true;
    else
      return false;
  };

  vm.totalItems;
  vm.currentPage = 1;
  vm.numPerPage = NUM_PER_PAGE;
  vm.filteredProducts;
  var currentPageBeforeSearch = null;

  /*vm.setPage = function (pageNo) {
    vm.currentPage = pageNo;
  };*/

  vm.pageChanged = function() {
    if(!currentPageBeforeSearch) {
      var begin = ((vm.currentPage - 1) * vm.numPerPage)
      , end = begin + vm.numPerPage;
      
      vm.filteredProducts = vm.products.slice(begin, end);
    }
  };

  vm.searchKeyword = function() {
    if(!vm.keyword) {
      vm.totalItems = vm.products.length;
      vm.numPerPage = NUM_PER_PAGE;
      vm.currentPage = currentPageBeforeSearch;
      currentPageBeforeSearch = null;
      vm.pageChanged();
    } else {
      if(!currentPageBeforeSearch)
        currentPageBeforeSearch = vm.currentPage;
      var tempProducts = [];
      vm.products.forEach(function(element, index, array) {
        if(element.pName.indexOf(vm.keyword) > -1)
          tempProducts.push(element);
      });
      vm.totalItems = vm.numPerPage = tempProducts.length;
      vm.filteredProducts = tempProducts;
    }
  };

  vm.orderBy;
  vm.order = function(value) {
    if(value == vm.orderBy)
      return;
    vm.orderBy = value;
    var tempProducts = [];
    if(currentPageBeforeSearch)
      tempProducts = vm.filteredProducts;
    else
      tempProducts = vm.products;

    tempProducts = _.chain(tempProducts).sortBy(function(item){
      if(value.indexOf("-") == 0)
        return -(item[value.substr(1)]);
      else
        return item[value];
    }).value();
    if(value == "-pBrand")
      tempProducts.reverse();

    vm.currentPage = 1;
    if(currentPageBeforeSearch)
      vm.filteredProducts = tempProducts;
    else
      vm.products = tempProducts
    vm.pageChanged();
  };
});

eatToDeadApp.controller("AddCtrl", function() {
  var vm = this;
});

eatToDeadApp.controller("ShowCtrl", function() {
  var vm = this;
  
  vm.brandClass = function(item) {
    var class_name = "label ";
    switch(item.pBrand) {
      case "統一":
        class_name += "label-danger";
        break;
      case "頂新":
        class_name += "label-info";
        break;
      case "味全":
        class_name += "label-warning";
        break;
      case "ゴミ丼":
        class_name += "label-primary";
        break;
      default:
        class_name += "label-default";
    }
    return class_name;
  };
});
